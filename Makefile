CC?=		cc
PROG=		xkillerd
SRCS=		xkillerd.c
OBJS=		$(SRCS:.c=.o)
CFLAGS+=	-c -Wall $(shell curl-config --cflags)
LDFLAGS+=	$(shell curl-config --libs) $(shell pkg-config --libs libbsd)

UNAME=		$(shell uname)
ifeq ($(UNAME), OpenBSD)
LDFLAGS+=	-lkvm
endif

DESTDIR?=
PREFIX?=	/usr/local

all: $(SRCS) $(PROG)

$(PROG): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -f $(OBJS) $(PROG)

.PHONY: install
install:
	install -s -m 0755 $(PROG) $(DESTDIR)$(PREFIX)/bin/$(PROG)

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(PROG)

.PHONY: check-syntax
check-syntax:
	$(CC) -o nul -S ${CHK_SOURCES}
