#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <time.h>
#include <syslog.h>
#include <string.h>
#include <ctype.h>
#include <err.h>
#include <curl/curl.h>

/* BSDs already have these functions */
#if !defined(__OpenBSD__) && !defined(__FreeBSD__) && !defined(__NetBSD__)
#include <bsd/stdlib.h>
#endif

/* kvm.h is something very different on Linux */
#ifdef __OpenBSD__
#include <kvm.h>
#include <sys/sysctl.h>
#include <sys/param.h>
#endif

#include "pathnames.h"

bool after_end(void);
time_t fetch_end(void);
void kill_x(void);
char *strstrip(char *);
size_t write_chunk(char *, size_t, size_t, char *);
void clean_up_and_exit(int);

int
main(int argc, char *argv[])
{
	pid_t pid = fork();
	if (pid < 0) {
		perror("fork");
		exit(EXIT_FAILURE);
	}
	/* parent */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}
	if (setsid() < 0) {
		perror("setsid");
		exit(EXIT_FAILURE);
	}
	signal(SIGCHLD, SIG_IGN);
	signal(SIGHUP, SIG_IGN);

	pid = fork();
	if (pid < 0) {
		perror("fork");
		exit(EXIT_FAILURE);
	}
	/* parent */
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}
	signal(SIGTERM, clean_up_and_exit);
	umask(0);
	chdir("/");
	syslog(LOG_NOTICE, "started");

	/* Kill all extant X sessions */
	kill_x();

	char formatted[64];
	time_t end = fetch_end();
	strftime(formatted, 64, "%Y-%m-%d %H:%M:%S", localtime(&end));
	syslog(LOG_NOTICE, "challenge will end at %s", formatted);

	while (true) {
		sleep(300);
		if (after_end()) {
			syslog(LOG_NOTICE, "reached end of challenge");
		} else {
			kill_x();
		}
	}
	clean_up_and_exit(SIGTERM);

	/* Should never get here */
	return (EXIT_SUCCESS);
}

/* Whether the time to stop killing has passed */
bool
after_end(void)
{
	time_t now = time(NULL);
	time_t end = fetch_end();
	return (difftime(now, end) < 0);
}

/* Fetches the end time from a server somewhere */
time_t
fetch_end(void)
{
	char *out = calloc(16, 1024);
	curl_global_init(CURL_GLOBAL_DEFAULT);
	CURL *curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, END_TIME_URL);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_chunk);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, out);
	CURLcode res = curl_easy_perform(curl);
	curl_easy_cleanup(curl);

	if (res != CURLE_OK) {
		errx(1, "curl failed: %d", res);
	}

	const char *errstr;
	time_t end = (time_t)strtonum(strstrip(out), 0, LONG_MAX, &errstr);
	if (errstr) {
		errx(1, "failed to parse time from server: %s", errstr);
	}
	return (end);
}

/* Writes a chunk of something to dst */
size_t
write_chunk(char *src, size_t size, size_t nmemb, char *dst)
{
	/* TODO: Make this safer or something */
	strcat(dst, src);
	return (size*nmemb);
}

/* Strips whitespace from a both sides of string */
char *
strstrip(char *s)
{
	char *e = s + strlen(s) - 1;
    while(*s && isspace(*s)) s++;
    while(e > s && isspace(*e)) *e-- = '\0';
    return (s);
}

/* Kills any X sessions - is there a portable way to do this? */
void
kill_x(void)
{
#   ifdef __linux
	/* Use libprocps here... */
#   elif __OpenBSD__
	char errbuf[_POSIX2_LINE_MAX];

	/* Open the currently running kernel image, not any file */
	kvm_t *kernel = kvm_openfiles(NULL, NULL, NULL, KVM_NO_FILES, errbuf);
	if (kernel == NULL) {
		perror("kvm_openfiles");
	}
	struct kinfo_proc *kinfo;
	int nentries = 0;

	/* See <sys/sysctl.h> for details on the kinfo_proc struct */
	kinfo = kvm_getprocs(kernel, KERN_PROC_ALL, 0, sizeof(*kinfo), &nentries);
	if (nentries == 0 || kinfo == NULL) {
		perror("kvm_getprocs");
		return;
	}
	int i;
	for (i = 0; i < nentries; i++) {
		char *name = kinfo[i].p_comm;
		if(strcmp(name, "Xorg") == NULL) {
			pid_t pid = kinfo[i].p_pid;
			if (kill(pid, SIGTERM) != 0) {
				syslog(LOG_INFO, "could not kill Xorg with PID %d", pid);
				perror("kill");
			} else {
				syslog(LOG_INFO, "killed Xorg with PID %d", pid);
			}
		}
	}
#   endif
}
	
/* Closes fds etc and exits */
void
clean_up_and_exit(int signum)
{
	syslog(LOG_NOTICE, "stopping");
	exit(EXIT_SUCCESS);
}

