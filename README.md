xkillerd
========
![Build Status](https://travis-ci.org/kaashif/xkillerd.svg)

A daemon that kills X sessions and queries a server for the time the
killing will stop. Then stops killing, obviously, when that time has
passed.

Installing
----------
Install libcurl then use `make` to build and install:

On Debian:

	$ sudo apt install libcurl4-dev

Then:

	$ make
	$ sudo make install

Running
-------
The program at /usr/local/bin/xkillerd (by default) is a normal
daemon. Add it to your /etc/rc.local, make a systemd unit file, start
it when you login, do whatever you want. You can start it manually
like this:
	
	$ xkillerd

And stop it like this:

	$ pkill xkillerd
	
